﻿using UnityEngine;
using System.Collections;

public class Manager : MonoBehaviour {
	public GameObject player;
	public Maze maze;

	private void Start () {
		maze.NewMaze ();
		maze.AddObject (player);
	}
}
