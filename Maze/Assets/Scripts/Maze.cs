﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Maze : MonoBehaviour {
	public int width, height;
	public GameObject wallPrefab;

	private byte[,] maze;
	private List<Vector3> pathMazes = new List<Vector3>();
	private Stack<Vector2> tileToTry = new Stack<Vector2>();
	private List<Vector2> offsets = new List<Vector2> { new Vector2(0, 1), new Vector2(0, -1), new Vector2(1, 0), new Vector2(-1, 0) };
	private System.Random rnd = new System.Random();
	private Vector2 currentTile;

	public Vector2 CurrentTile {
		get { return currentTile; }
		private set {
			if (value.x % 2 == 1 || value.y % 2 == 1) {
				currentTile = value;
			}
		}
	}

	public void NewMaze() {
		GenerateRandomMaze();
	}

	// Reset maze by destroying all walls
	// A better solution would be to pool the wall objects
	public void Reset() {
		foreach (Transform child in transform) {
			Destroy(child.gameObject);
		}

		NewMaze ();
	}

	private void GenerateRandomMaze() {
		maze = new byte[width, height];

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				maze[x, y] = 1;
			}
		}

		CurrentTile = Vector2.one;
		tileToTry.Push(CurrentTile);
		maze = CreateMaze();

		SpawnWalls ();
	}

	// Loop through the array and instantiate a wall object where ever there is a 1 in the array
	private void SpawnWalls() {
		for (int i = 0; i <= maze.GetUpperBound(0); i++) {
			for (int j = 0; j <= maze.GetUpperBound(1); j++) {
				if (maze[i, j] == 1) {
					Vector2 pos = new Vector2(i, j);
					GameObject wall = (GameObject)GameObject.Instantiate(wallPrefab, pos, Quaternion.Euler(new Vector3(270, 0, 0)));
					wall.transform.parent = transform;
				} else if (maze[i, j] == 0) {
					pathMazes.Add(new Vector3(i, j, 0));
				}

			}
		}
	}

	private byte[,] CreateMaze() {
		//local variable to store neighbors to the current square as we work our way through the maze
		List<Vector2> neighbors;

		//as long as there are still tiles to try
		while (tileToTry.Count > 0) {
			//excavate the square we are on
			maze[(int)CurrentTile.x, (int)CurrentTile.y] = 0;

			//get all valid neighbors for the new tile
			neighbors = GetValidNeighbors(CurrentTile);

			//if there are any interesting looking neighbors
			if (neighbors.Count > 0) {
				//remember this tile, by putting it on the stack
				tileToTry.Push(CurrentTile);
				//move on to a random of the neighboring tiles
				CurrentTile = neighbors[rnd.Next(neighbors.Count)];
			} else {
				//if there were no neighbors to try, we are at a dead-end
				//toss this tile out
				//(thereby returning to a previous tile in the list to check).
				CurrentTile = tileToTry.Pop();
			}
		}

		return maze;
	}

	// Get all the prospective neighboring tiles
	// Returns all and any valid neighbors
	private List<Vector2> GetValidNeighbors(Vector2 centerTile) {
		List<Vector2> validNeighbors = new List<Vector2>();

		//Check all four directions around the tile
		foreach (Vector2 offset in offsets) {
			//find the neighbor's position
			Vector2 toCheck = new Vector2(centerTile.x + offset.x, centerTile.y + offset.y);

			//make sure the tile is not on both an even X-axis and an even Y-axis
			//to ensure we can get walls around all tunnels
			if (toCheck.x % 2 == 1 || toCheck.y % 2 == 1) {
				//if the potential neighbor is unexcavated (==1)
				//and still has three walls intact (new territory)
				if (maze[(int)toCheck.x, (int)toCheck.y] == 1 && HasThreeWallsIntact(toCheck)) {
					//add the neighbor
					validNeighbors.Add(toCheck);
				}
			}
		}

		return validNeighbors;
	}

	// Counts the number of intact walls around a tile
	// Provide the coordinates of the tile to check
	// Returns whether there are three intact walls (the tile has not been dug into earlier)
	private bool HasThreeWallsIntact(Vector2 Vector2ToCheck) {
		int intactWallCounter = 0;

		//Check all four directions around the tile
		foreach (Vector2 offset in offsets) {
			//find the neighbor's position
			Vector2 neighborToCheck = new Vector2(Vector2ToCheck.x + offset.x, Vector2ToCheck.y + offset.y);

			//make sure it is inside the maze, and it hasn't been dug out yet
			if (IsInside(neighborToCheck) && maze[(int)neighborToCheck.x, (int)neighborToCheck.y] == 1) {
				intactWallCounter++;
			}
		}

		//tell whether three walls are intact
		return intactWallCounter == 3;
	}

	// Check if it is inside the bounds of the maze
	private bool IsInside(Vector2 p) {
		return p.x >= 0 && p.y >= 0 && p.x < width && p.y < height;
	}

	// Add a gameObject to a random position in the maze but not on the player
	public void AddObject (GameObject obj) {
		bool isValid = false;

		do {
			int x = rnd.Next (width);
			int y = rnd.Next (height);

			Vector2 pos = new Vector2(x, y);

			if (maze [x, y] == 0) {
				obj.transform.position = new Vector2(x, y);
				isValid = true;
			}
		} while (!isValid);
	}
}